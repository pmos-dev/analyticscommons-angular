/*
 * Public API Surface of ngx-analyticscommons-live
 */

export * from './lib/services/analytics-live.service';

export * from './lib/ngx-analyticscommons-live.module';
