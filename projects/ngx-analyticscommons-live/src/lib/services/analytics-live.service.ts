import { EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsType } from 'tscommons-core';
import { TPropertyObject } from 'tscommons-core';
import { TEncodedObject } from 'tscommons-core';
import { IAnalyticsLiveHit, isIAnalyticsLiveHit } from 'tscommons-analytics';
import { IAnalyticsLiveAssociate, isIAnalyticsLiveAssociate } from 'tscommons-analytics';

import { CommonsSocketIoClientService } from 'ngx-httpcommons-socket-io';
import { CommonsSocketIoUrlOptions } from 'ngx-httpcommons-socket-io';

export abstract class AnalyticsLiveService<L extends IAnalyticsLiveHit> extends CommonsSocketIoClientService {
	private onHit: EventEmitter<L> = new EventEmitter<L>(true);
	private onAssociate: EventEmitter<IAnalyticsLiveAssociate> = new EventEmitter<IAnalyticsLiveAssociate>(true);
	private onChain: EventEmitter<L> = new EventEmitter<L>(true);

	constructor(
			baseUrl: string
	) {
		super(
				CommonsSocketIoUrlOptions.buildUrl(baseUrl),
				true,
				CommonsSocketIoUrlOptions.buildOptions(baseUrl)
		);

		this.connect();
	}

	protected abstract isValidLive(data: IAnalyticsLiveHit): boolean;
	
	protected setupOns(): void {
		super.on('hit', (data: unknown): void => {
			if (!CommonsType.isEncodedObject(data)) throw new Error('Unable to parse hit');
			const encoded: TEncodedObject = data as TEncodedObject;
			const object: TPropertyObject = CommonsType.decodePropertyObject(encoded);
			
			if (!isIAnalyticsLiveHit(object)) return;
			if (!this.isValidLive(object)) return;

			this.onHit.emit(object as L);
		});

		super.on('associate', (data: unknown): void => {
			if (!CommonsType.isEncodedObject(data)) throw new Error('Unable to parse hit');
			if (!isIAnalyticsLiveAssociate(data)) return;
			
			this.onAssociate.emit(data);
		});

		super.on('chain', (data: unknown): void => {
			if (!CommonsType.isEncodedObject(data)) throw new Error('Unable to parse hit');
			const encoded: TEncodedObject = data as TEncodedObject;
			const object: TPropertyObject = CommonsType.decodePropertyObject(encoded);
			
			if (!isIAnalyticsLiveHit(object)) return;
			if (!this.isValidLive(object)) return;

			this.onChain.emit(object as L);
		});
	}

	public hitObservable(): Observable<L> {
		return this.onHit;
	}

	public associateObservable(): Observable<IAnalyticsLiveAssociate> {
		return this.onAssociate;
	}

	public chainObservable(): Observable<L> {
		return this.onChain;
	}
}
