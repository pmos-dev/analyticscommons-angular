import { EAnalyticsTimePeriod } from '../enums/eanalytics-time-period';
import { EAnalyticsGrouping } from '../enums/eanalytics-grouping';

import { IAnalyticsTimePeriods } from './ianalytics-time-periods';

export interface IAnalyticsHitsTally extends IAnalyticsTimePeriods<number> {
		[EAnalyticsTimePeriod.TOTAL]: number|undefined;
		[EAnalyticsTimePeriod.TODAY]: number|undefined;
		[EAnalyticsTimePeriod.HOUR]: number|undefined;
}

export interface IAnalyticsAllHitsTally {
		[EAnalyticsGrouping.TOTAL]: IAnalyticsHitsTally;
		[EAnalyticsGrouping.UNIQUE]: IAnalyticsHitsTally;
		[EAnalyticsGrouping.UNKNOWN]: IAnalyticsHitsTally;
}
