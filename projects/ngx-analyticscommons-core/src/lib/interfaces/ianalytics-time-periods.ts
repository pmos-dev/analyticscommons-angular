export interface IAnalyticsTimePeriods<T> {
		[period: string]: T|undefined;
}
