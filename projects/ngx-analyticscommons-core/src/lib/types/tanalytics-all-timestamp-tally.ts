import { CommonsType } from 'tscommons-core';

import { TAnalyticsTimestampTally } from 'tscommons-analytics';
	
import { EAnalyticsGrouping } from '../enums/eanalytics-grouping';

export type TAnalyticsAllTimestampTally = {
		[EAnalyticsGrouping.TOTAL]: TAnalyticsTimestampTally[];
		[EAnalyticsGrouping.UNIQUE]: TAnalyticsTimestampTally[];
		[EAnalyticsGrouping.UNKNOWN]: TAnalyticsTimestampTally[];
};
