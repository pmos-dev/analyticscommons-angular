import { CommonsType } from 'tscommons-core';

export enum EAnalyticsGrouping {
		TOTAL = 'total',
		UNIQUE = 'unique',
		UNKNOWN = 'unknown'
}

export function toEAnalyticsGrouping(type: string): EAnalyticsGrouping|undefined {
	switch (type) {
		case EAnalyticsGrouping.TOTAL.toString():
			return EAnalyticsGrouping.TOTAL;
		case EAnalyticsGrouping.UNIQUE.toString():
			return EAnalyticsGrouping.UNIQUE;
		case EAnalyticsGrouping.UNKNOWN.toString():
			return EAnalyticsGrouping.UNKNOWN;
	}
	return undefined;
}

export function fromEAnalyticsGrouping(type: EAnalyticsGrouping): string {
	switch (type) {
		case EAnalyticsGrouping.TOTAL:
			return EAnalyticsGrouping.TOTAL.toString();
		case EAnalyticsGrouping.UNIQUE:
			return EAnalyticsGrouping.UNIQUE.toString();
		case EAnalyticsGrouping.UNKNOWN:
			return EAnalyticsGrouping.UNKNOWN.toString();
	}
	
	throw new Error('Unknown EAnalyticsGrouping');
}

export function isEAnalyticsGrouping(test: unknown): test is EAnalyticsGrouping {
	if (!CommonsType.isString(test)) return false;
	
	return toEAnalyticsGrouping(test) !== undefined;
}

export function keyToEAnalyticsGrouping(key: string): EAnalyticsGrouping {
	switch (key) {
		case 'TOTAL':
			return EAnalyticsGrouping.TOTAL;
		case 'UNIQUE':
			return EAnalyticsGrouping.UNIQUE;
		case 'UNKNOWN':
			return EAnalyticsGrouping.UNKNOWN;
	}
	
	throw new Error(`Unable to obtain EAnalyticsGrouping for key: ${key}`);
}

export const EANALYTICS_GROUPINGS: EAnalyticsGrouping[] = Object.keys(EAnalyticsGrouping)
		.map((e: string): EAnalyticsGrouping => keyToEAnalyticsGrouping(e));
