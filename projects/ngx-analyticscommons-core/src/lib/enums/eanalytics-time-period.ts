import { CommonsType } from 'tscommons-core';

export enum EAnalyticsTimePeriod {
		TOTAL = 'total',
		TODAY = 'today',
		HOUR = 'hour'
}

export function toEAnalyticsTimePeriod(type: string): EAnalyticsTimePeriod|undefined {
	switch (type) {
		case EAnalyticsTimePeriod.TOTAL.toString():
			return EAnalyticsTimePeriod.TOTAL;
		case EAnalyticsTimePeriod.TODAY.toString():
			return EAnalyticsTimePeriod.TODAY;
		case EAnalyticsTimePeriod.HOUR.toString():
			return EAnalyticsTimePeriod.HOUR;
	}
	return undefined;
}

export function fromEAnalyticsTimePeriod(type: EAnalyticsTimePeriod): string {
	switch (type) {
		case EAnalyticsTimePeriod.TOTAL:
			return EAnalyticsTimePeriod.TOTAL.toString();
		case EAnalyticsTimePeriod.TODAY:
			return EAnalyticsTimePeriod.TODAY.toString();
		case EAnalyticsTimePeriod.HOUR:
			return EAnalyticsTimePeriod.HOUR.toString();
	}
	
	throw new Error('Unknown EAnalyticsTimePeriod');
}

export function isEAnalyticsTimePeriod(test: unknown): test is EAnalyticsTimePeriod {
	if (!CommonsType.isString(test)) return false;
	
	return toEAnalyticsTimePeriod(test) !== undefined;
}

export function keyToEAnalyticsTimePeriod(key: string): EAnalyticsTimePeriod {
	switch (key) {
		case 'TOTAL':
			return EAnalyticsTimePeriod.TOTAL;
		case 'TODAY':
			return EAnalyticsTimePeriod.TODAY;
		case 'HOUR':
			return EAnalyticsTimePeriod.HOUR;
	}
	
	throw new Error(`Unable to obtain EAnalyticsTimePeriod for key: ${key}`);
}

export const EANALYTICS_TIME_PERIODS: EAnalyticsTimePeriod[] = Object.keys(EAnalyticsTimePeriod)
		.map((e: string): EAnalyticsTimePeriod => keyToEAnalyticsTimePeriod(e));
