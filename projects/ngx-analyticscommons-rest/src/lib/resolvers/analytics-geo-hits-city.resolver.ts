import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsGeoHitCity } from 'tscommons-analytics';

export class AnalyticsGeoHitsCityResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsGeoHitCity[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsGeoHitCity[]> {
		return this.restService.listGeoHitsCityUk(100);
	}
}
