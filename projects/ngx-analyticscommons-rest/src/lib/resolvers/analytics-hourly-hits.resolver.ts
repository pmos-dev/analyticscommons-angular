import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsTimestampTally } from 'tscommons-analytics';

import { EAnalyticsGrouping } from 'ngx-analyticscommons-core';
import { TAnalyticsAllTimestampTally } from 'ngx-analyticscommons-core';

export class AnalyticsHourlyHitsResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsAllTimestampTally> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	public static async getAllHits<H extends IAnalyticsHit>(restService: AnalyticsRestClientService<H>): Promise<TAnalyticsAllTimestampTally> {
		const promises: Promise<TAnalyticsTimestampTally[]>[] = [
				restService.listHourlyTotalHits(),
				restService.listHourlyUniqueHits(),
				restService.listHourlyUnknownHits()
		];
		
		const results: TAnalyticsTimestampTally[][] = await Promise.all(promises);

		return {
				[EAnalyticsGrouping.TOTAL]: results[0],
				[EAnalyticsGrouping.UNIQUE]: results[1],
				[EAnalyticsGrouping.UNKNOWN]: results[2]
		};
	}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsAllTimestampTally> {
		return AnalyticsHourlyHitsResolver.getAllHits<H>(this.restService);
	}
}
