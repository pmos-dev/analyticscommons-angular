import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsDeviceVersionedHits } from 'tscommons-analytics';

export class AnalyticsDeviceEngineHitsResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsDeviceVersionedHits[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsDeviceVersionedHits[]> {
		return this.restService.listDeviceEngineHits();
	}
}
