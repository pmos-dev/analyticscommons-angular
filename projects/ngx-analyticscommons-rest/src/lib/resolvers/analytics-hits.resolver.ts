import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';

import { EAnalyticsTimePeriod } from 'ngx-analyticscommons-core';
import { EAnalyticsGrouping } from 'ngx-analyticscommons-core';

import { IAnalyticsHitsTally, IAnalyticsAllHitsTally } from 'ngx-analyticscommons-core';

export class AnalyticsHitsResolver<H extends IAnalyticsHit> implements Resolve<IAnalyticsAllHitsTally> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	public static async getHits<H extends IAnalyticsHit>(group: EAnalyticsGrouping, restService: AnalyticsRestClientService<H>): Promise<IAnalyticsHitsTally> {
		const promises: Promise<number>[] = [];

		const to: Date = new Date();
		
		const hour: Date = new Date(to.getTime());
		hour.setHours(hour.getHours() - 1);
		
		const day: Date = new Date(to.getTime());
		hour.setHours(0);

		switch (group) {
			case EAnalyticsGrouping.TOTAL:
				promises.push(restService.getTotalHits());
				promises.push(restService.getTotalHits({ from: day, to: to }));
				promises.push(restService.getTotalHits({ from: hour, to: to }));
				break;
			case EAnalyticsGrouping.UNIQUE:
				promises.push(restService.getUniqueHits());
				promises.push(restService.getUniqueHits({ from: day, to: to }));
				promises.push(restService.getUniqueHits({ from: hour, to: to }));
				break;
			case EAnalyticsGrouping.UNKNOWN:
				promises.push(restService.getUnknownHits());
				promises.push(restService.getUnknownHits({ from: day, to: to }));
				promises.push(restService.getUnknownHits({ from: hour, to: to }));
				break;
		}
		
		const results: number[] = await Promise.all(promises);
		
		return {
				[EAnalyticsTimePeriod.TOTAL]: results[0],
				[EAnalyticsTimePeriod.TODAY]: results[1],
				[EAnalyticsTimePeriod.HOUR]: results[2]
		};
	}
	
	public static async getAllHits<H extends IAnalyticsHit>(restService: AnalyticsRestClientService<H>): Promise<IAnalyticsAllHitsTally> {
		const promises: Promise<IAnalyticsHitsTally>[] = [
				AnalyticsHitsResolver.getHits<H>(EAnalyticsGrouping.TOTAL, restService),
				AnalyticsHitsResolver.getHits<H>(EAnalyticsGrouping.UNIQUE, restService),
				AnalyticsHitsResolver.getHits<H>(EAnalyticsGrouping.UNKNOWN, restService)
		];
		
		const results: IAnalyticsHitsTally[] = await Promise.all(promises);

		return {
				[EAnalyticsGrouping.TOTAL]: results[0],
				[EAnalyticsGrouping.UNIQUE]: results[1],
				[EAnalyticsGrouping.UNKNOWN]: results[2]
		};
	}
	
	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<IAnalyticsAllHitsTally> {
		return AnalyticsHitsResolver.getAllHits<H>(this.restService);
	}
}
