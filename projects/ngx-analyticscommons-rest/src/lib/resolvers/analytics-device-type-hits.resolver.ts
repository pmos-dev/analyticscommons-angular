import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsDeviceTypeHits } from 'tscommons-analytics';

export class AnalyticsDeviceTypeHitsResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsDeviceTypeHits[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsDeviceTypeHits[]> {
		return this.restService.listDeviceTypeHits();
	}
}
