import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsGeoHitCountry } from 'tscommons-analytics';

export class AnalyticsGeoHitsCountryResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsGeoHitCountry[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsGeoHitCountry[]> {
		return this.restService.listGeoHitsCountryUk(100);
	}
}
