import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsDeviceCpuHits } from 'tscommons-analytics';

export class AnalyticsDeviceCpuHitsResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsDeviceCpuHits[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsDeviceCpuHits[]> {
		return this.restService.listDeviceCpuHits();
	}
}
