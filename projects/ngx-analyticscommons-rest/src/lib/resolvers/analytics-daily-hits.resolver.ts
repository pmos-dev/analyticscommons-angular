import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsTimestampTally } from 'tscommons-analytics';

import { EAnalyticsGrouping } from 'ngx-analyticscommons-core';
import { TAnalyticsAllTimestampTally } from 'ngx-analyticscommons-core';

export abstract class AnalyticsDailyHitsResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsAllTimestampTally> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	public static async getAllHits<H extends IAnalyticsHit>(restService: AnalyticsRestClientService<H>): Promise<TAnalyticsAllTimestampTally> {
		const promises: Promise<TAnalyticsTimestampTally[]>[] = [
				restService.listDailyTotalHits(),
				restService.listDailyUniqueHits(),
				restService.listDailyUnknownHits()
		];
		
		const results: TAnalyticsTimestampTally[][] = await Promise.all(promises);

		return {
				[EAnalyticsGrouping.TOTAL]: results[0],
				[EAnalyticsGrouping.UNIQUE]: results[1],
				[EAnalyticsGrouping.UNKNOWN]: results[2]
		};
	}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsAllTimestampTally> {
		return AnalyticsDailyHitsResolver.getAllHits<H>(this.restService);
	}
}
