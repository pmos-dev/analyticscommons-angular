import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsGeoAggregate } from 'tscommons-analytics';

export class AnalyticsMapWorldResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsGeoAggregate[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsGeoAggregate[]> {
		return this.restService.listGeoAggregatesWorld(1);
	}
}
