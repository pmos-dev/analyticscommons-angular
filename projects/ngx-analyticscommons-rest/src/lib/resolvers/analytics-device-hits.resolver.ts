import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { AnalyticsRestClientService } from 'tscommons-analytics';
import { IAnalyticsHit } from 'tscommons-analytics';
import { TAnalyticsDeviceHits } from 'tscommons-analytics';

export class AnalyticsDeviceHitsResolver<H extends IAnalyticsHit> implements Resolve<TAnalyticsDeviceHits[]> {
	constructor(
			private restService: AnalyticsRestClientService<H>
	) {}

	resolve(
			_route: ActivatedRouteSnapshot,
			_state: RouterStateSnapshot
	): Promise<TAnalyticsDeviceHits[]> {
		return this.restService.listDeviceHits();
	}
}
